export const userName = 'opianalytics';
export const apy_key = 'c-Bzli96HYEOIDqxtAeB3A';
export const basemapURL = 'https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}.png';
export const table_name = 'puntos_examen_frontend';
export const columnsInformation = [
    { field: 'totalPuntos', headerName: 'Total de puntos'},
    { field: 'totalSucursales', headerName: 'Total de sucursales'},
    { field: 'totalCompetidores', headerName: 'Total de competidores'},
    { field: 'totalOtros', headerName: 'Total de otros'},
    { field: 'porcetajePuntosSuc', headerName: 'Porcentaje de puntos de sucursales'},
    { field: 'porcetajePuntosComp', headerName: 'Porcentaje de puntos de competidores'},
    { field: 'porcentajeOtros', headerName: 'Porcentaje de otros'}
  ];
export const texts = {
    sucursal: 'Sucursal',
    competidor: 'Competidor',
    buttons: {
        obtenerPuntos: 'Obtener puntos',
        obtenerSucursales: 'Obtener sucursales',
        obtenerCompetidores: 'Obtener competidores',
        obtenerTodosSinSucCompetidores: 'Obtener todos distintos de competidores y sucursales'

    },
    alert: {
        messageError: 'No se obtuvieron los resultados, intentalo de nuevo por favor.',
        success: 'Se obtuvieron los resultados satisfactoriamente.',
    },
    titles: {
        informacionGeneral: 'Información General',
        informacionRegion: 'Información de la Región Visible'
    }
}
export const alert = {
    types: {
        success: 'success',
        error: 'error'
    }
}

export const requestType = {
    todos: 'todos',
    sucursales: 'soloSucursales',
    competidores: 'soloCompetidores',
    otrosDistSucComp: 'otros'
}

export const querys = {
    todos: `SELECT * FROM ${table_name}`,
    soloSucursales: `SELECT * FROM ${table_name} WHERE tipo = '${texts.sucursal}'`,
    soloCompetidores: `SELECT * FROM ${table_name} WHERE tipo = '${texts.competidor}'`,
    otros: `SELECT * FROM ${table_name} WHERE tipo <> '${texts.sucursal}' AND tipo <> '${texts.competidor}'`
}

export const button = {
    colors: {
        primary: 'primary',
        secondary: 'secondary'
    }
}


