import React, {useEffect} from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
}));

function SimpleSpinner(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    useEffect(() => setOpen(props.show), [props.show]) 
    return (
        <div>
            <Backdrop className={classes.backdrop} open={open} transitionDuration={500}>
                <CircularProgress color="inherit" />
            </Backdrop>
        </div>
        
    )
}

export default SimpleSpinner;