import React, { useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';
import { alert } from './../../../globals';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

function SimpleAlert (props) {
    const classes = useStyles();
    const [show, setShow] = useState(props.show);
    const [message, setMessage] = useState('');
    const [alertType, setAlertType] = useState(alert.types.success);

    useEffect(() => {
      setShow(props.show)
      setMessage(props.message)
    }, [props.show, props.message]);

    useEffect(() => {
      const alT = props.alertType && props.alertType  !== '' ? alert.types[props.alertType] : alert.types.success;
      setAlertType(alT);
    }, [props.alertType])

    return (
      <div className={classes.root}>
        <Collapse in={show}>
          <Alert
            action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setShow(false);
                  }}
                >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            severity={alertType}
          >
            {message}
          </Alert>
        </Collapse>
      </div>
    )
}

export default SimpleAlert;
