import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function SimpleTable (props) {
  const classes = useStyles();
  const simpleTableTemplate =
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
        <TableRow >
          {props.columns && props.columns.map((column, i) => (
              <TableCell key={i} align="center">{column.headerName}</TableCell>
          ))}
        </TableRow>
        </TableHead>
        <TableBody>
          {props.rows && props.rows.map( (row, i) => (
            <TableRow key={i}>
              {props.columns && props.columns.map( (column, j) => (
                  <TableCell align="center" component="th" scope="row" key={j}>
                     {row[column.field]}
                  </TableCell>
               ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>

    return (
      <div>
        {props.columns && props.rows && simpleTableTemplate}
      </div>
    )
}

export default SimpleTable;
