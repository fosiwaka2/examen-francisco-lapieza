import React, { useEffect, useRef } from 'react';
import * as L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import PropTypes from 'prop-types';
import './Map.css';
import {basemapURL} from './../../../globals';

function Map (props) {
    const {
        lat,
        lng,
        zoom,
        basemapURL: baseMapURL,
        coordinates
    } = props;
    const mapRef = useRef(null);
    const circleRef = useRef(null);
    useEffect(() => {
        mapRef.current = L.map('map', {
            center: [lat, lng],
            zoom,
            zoomControl: false
        });
        L.tileLayer(baseMapURL, {
            detectRetina: true,
            retina: '@2x',
        }).addTo(mapRef.current);
    }, [
        lat,
        lng,
        zoom,
        baseMapURL
    ]);

    useEffect(() => {
        clearMap();
        coordinates?.forEach(elementCoordinate => {
            circleRef.current = L.circle([elementCoordinate.latitude, elementCoordinate.longitude], {
                color: `#${elementCoordinate.color}`,
                radius: 3000
            }).addTo(mapRef.current);
        });

        mapRef.current.on('moveend', function(e) {
            setTimeout(() => {
                getVisibleCircleMarkers();
            }, 300);
        });
        setTimeout(() => {
            getVisibleCircleMarkers();
        }, 100);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [coordinates])

    const getVisibleCircleMarkers = () => {
        let  visibleMarkers = [];
        let  bounds = mapRef.current.getBounds();
        coordinates && coordinates.length > 0 && mapRef.current.eachLayer((l) => {
            if( l instanceof L.CircleMarker && bounds.contains(l.getLatLng()) ) {
                visibleMarkers.push(l)
            }
       })
       props.onMoveend(visibleMarkers);
    }

    function clearMap() {
        for(const i in mapRef.current._layers) {
            if(mapRef.current._layers[i]._path !== undefined) {
                try {
                    mapRef.current.removeLayer(mapRef.current._layers[i]);
                }
                catch(e) {
                    console.log("problem with " + e + mapRef.current._layers[i]);
                }
            }
        }
    }

    return (
        <div id="map"/>
    );
}

Map.propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    zoom: PropTypes.number,
    basemapURL: PropTypes.string,


};
Map.defaultProps = {
    zoom: 13,
    basemapURL,

}

export default Map;
