import React from 'react';
// import PropTypes from 'prop-types';
import ButtonElemet from '@material-ui/core/Button';
import './Button.css';


function Button (props) {
    const onClickButton = () => {
        props.onClickButton();
    }
    return (
        <ButtonElemet
            style={props.specialStyle}
            disabled={props.disabled}
            onClick={onClickButton}
            className="points-button"
            color={props.color}
            variant="contained">
            {props.textButton}
        </ButtonElemet>
    );
}

export default Button;
