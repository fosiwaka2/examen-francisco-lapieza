export const getPorcentaje = (total, number) => {
    const porcentaje = (100*number/total).toFixed(2);
    const result = total && number ? `${porcentaje}%` : '0%';
    return result;
}
