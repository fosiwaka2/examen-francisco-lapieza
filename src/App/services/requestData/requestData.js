import axios from 'axios';
import {getPorcentaje} from './../utils/utils';
import {userName, apy_key, texts} from './../../../globals';

export const requestCoordinates = (query) => {
    const request = `https://${userName}.carto.com/api/v2/sql?api_key=${apy_key}&q=${query}`;
    return new Promise((resolve, reject) => {
        axios.get(request).then(
            responseCoordinates => {
              const reqCoordinatesClear = responseCoordinates.data.rows;
              const resolveReq = {
                reqCoordinatesClear,
                responseCoordinates: mapInformation(responseCoordinates), 
              }
              resolve(resolveReq);
            }
          ).catch( error => reject(error) )
    });
  }

const mapInformation = (responseCoordinates) => {
    const totalPuntos = responseCoordinates.data.total_rows;
    let totalSucursales = 0;
    let totalCompetidores = 0;
    let totalOtros = 0;

    responseCoordinates.data.rows.forEach((row) => {
      row.tipo === texts.sucursal ? totalSucursales++ :
      row.tipo === texts.competidor ? totalCompetidores++ : totalOtros++;
    })

    const rC = {
      totalPuntos,
      totalSucursales,
      totalCompetidores,
      totalOtros,
      porcetajePuntosSuc: getPorcentaje(totalPuntos, totalSucursales),
      porcetajePuntosComp: getPorcentaje(totalPuntos, totalCompetidores),
      porcentajeOtros: getPorcentaje(totalPuntos, totalOtros),
    };
    return [rC];
  }