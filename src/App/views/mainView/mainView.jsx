import React, { useState } from 'react';
import Map from '../../components/map/Map.jsx';
import Button from '../../components/button/Button.jsx';
import './mainView.css';
import SimpleSpinner from '../../components/simpleSpinner/simpleSpinner';
import SimpleTable from '../../components/simpleTable/simpleTable.jsx';
import {requestCoordinates} from './../../services/requestData/requestData';
import { columnsInformation, texts, alert, requestType, querys, button } from './../../../globals';
import SimpleAlert from './../../components/simpleAlert/simpleAlert.jsx';
import { getPorcentaje } from './../../services/utils/utils';

function MainView() {
  const [coordinates, setCoordinates] = useState(null);
  const [searching, setSearching] = useState(false);
  const [rows, setRows] = useState(null);
  const [rows2, setRows2] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
  const [messageAlert, setMessageAlert] = useState(null);
  const [alertType, setAlertType] = useState('');
  const [visibleMarkersC, setVisibleMarkersC] = useState(null);

  const requestCoord = (typeRequest) => {
    const query = querys[typeRequest]
    setCoordinates(null);
    setRows(null);
    setRows2(null);
    setSearching(true);
    setShowAlert(false);
    setMessageAlert(null);
    requestCoordinates(query).then( response => {
        const visM = getVisibleMarkers(visibleMarkersC);
        setCoordinates(response.reqCoordinatesClear);
        setRows(response.responseCoordinates);
        visM ? setRows2([visM]) : setRows2(response.responseCoordinates);
        setSearching(false);
        setMessageAlert(texts.alert.success);
        setShowAlert(true);
        setAlertType(alert.types.success);
    }).catch(() => {
      setSearching(false);
      setShowAlert(true);
      setMessageAlert(texts.alert.messageError);
      setAlertType(alert.types.error);
    });
  }

  const onMooved = (visibleMarkers) => {
    setVisibleMarkersC(visibleMarkers);
    setRows2([getVisibleMarkers(visibleMarkers)]);
  }

  const getVisibleMarkers = (visibleMarkers) => {
    let informationVisibleMarkers;
    const totalPuntos = visibleMarkers?.length;
    const totalSucursales = visibleMarkers?.filter(marker => marker.options.color === '#11A579').length;
    const totalCompetidores = visibleMarkers?.filter(marker => marker.options.color === '#3969AC').length;
    const totalOtros = totalPuntos - totalSucursales - totalCompetidores;
    if (visibleMarkers) {
      informationVisibleMarkers = {
        totalPuntos: typeof totalPuntos === 'number' ? totalPuntos : '-',
        totalSucursales: typeof totalSucursales === 'number' ? totalSucursales : '-',
        totalCompetidores: typeof totalCompetidores === 'number' ? totalCompetidores : '-',
        totalOtros: typeof totalOtros === 'number' ? totalOtros : '-',
        porcetajePuntosSuc: getPorcentaje(totalPuntos, totalSucursales),
        porcetajePuntosComp: getPorcentaje(totalPuntos, totalCompetidores),
        porcentajeOtros: getPorcentaje(totalPuntos, totalOtros),
      }
    }
    return informationVisibleMarkers;
  }

  return (
    <div id="app">
      <SimpleSpinner show={searching}/>
      <div className="size-width-all">
        <SimpleAlert message={messageAlert} show={showAlert} alertType={alertType}/>
        
      </div>
      {rows && rows2 &&
        (
          <div className="size-width-all">
            <h3>{texts.titles.informacionGeneral}</h3>
            <SimpleTable columns={columnsInformation} rows={rows}></SimpleTable>
            <h3>{texts.titles.informacionRegion}</h3>
            <SimpleTable columns={columnsInformation} rows={rows2}></SimpleTable>
          </div>
        )
      }
      <Map
        coordinates={coordinates}
        zoom={5}
        lat={19.453603}
        lng={-99.140410}
        onMoveend={onMooved}/>
      <div>
        <Button
          specialStyle={{bottom: 175}}
          color={button.colors.secondary}
          textButton={texts.buttons.obtenerSucursales}
          onClickButton={() => requestCoord(requestType.sucursales)} disabled={searching}/>
        <Button
          specialStyle={{bottom: 125}}
          color={button.colors.secondary}
          textButton={texts.buttons.obtenerCompetidores}
          onClickButton={() => requestCoord(requestType.competidores)} disabled={searching}/>
        <Button
          specialStyle={{bottom: 75}}
          color={button.colors.secondary}
          textButton={texts.buttons.obtenerTodosSinSucCompetidores}
          onClickButton={() => requestCoord(requestType.otrosDistSucComp)} disabled={searching}/>
        <Button
          specialStyle={{bottom: 25}}
          color={button.colors.primary}
          textButton={texts.buttons.obtenerPuntos}
          onClickButton={() => requestCoord(requestType.todos)} disabled={searching}/>
      </div>
    </div>
  );
}

export default MainView;
