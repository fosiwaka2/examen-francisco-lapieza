import React from 'react';
import './App.css';
import MainView from './views/mainView/mainView';

function App() {

  return (
    <MainView/>
  );
}

export default App;
